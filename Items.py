class Item:
    __id = 1

    def __init__(self, title, price, description):
        Item.__id += 1
        self.__id = Item.__id
        self.title = title
        self.price = price
        self.description = description

    def __str__(self):
        return self.title


class ItemList:

    def __init__(self, itemList, catalog):
        self.__last_id = len(itemList) + 1
        self.itemList = {}
        for id in range(len(itemList)):
            self.itemList[id + 1] = [catalog, itemList[id]]

    def add_itemList(self, itemList, catalog):
        for i in range(0, len(itemList)):
            id = self.__last_id
            self.itemList[id] = [catalog, itemList[i]]
            self.__last_id += 1

    def sell_item(self, item):
        out = []
        for key in self.itemList.keys():
            if self.itemList[key][1] == item:
                out.append(key)
        for id in out:
            del self.itemList[id]

    def is_item_in_shop(self, item):
        for key in self.itemList.keys():
            if self.itemList[key][1] == item:
                return True
        return False

    def get_all_item_in_catalog(self, catalog):
        res = []
        for key in self.itemList.keys():
            if self.itemList[key][0] == catalog:
                res.append(self.itemList[key][1])
        return res

    def print_all_list(self):
        print('_' * 12)
        print("Весь список товаров в магазине:")
        for id, v in self.itemList.items():
            print(id, ':', v[1].title, v[1].price, 'руб')
            print('Catalog:', v[0].title)
            print()


class Catalog:
    __id = 0

    def __init__(self, title, catalog=None):
        Catalog.__id += 1
        self.__id = Catalog.__id
        self.catalog = catalog
        self.title = title
