class User:
    __id = 0

    def __init__(self, name, surname, address, itemList):
        User.__id += 1
        self.__id = User.__id
        self.name = name
        self.surname = surname
        self.address = address
        self.__itemList = itemList

    def __str__(self):
        return str(self.__id) + ' ' + self.name + ' ' + self.surname

    def get_info(self):
        return self.__id, self.name, self.surname, self.address.get_info()

    # def buy_item(self, item):
    #     order = Order(self, item, self.__itemList)


class Admin(User):

    def change_item(self, item, new_title, new_catalog):
        pass


class Address:

    def __init__(self, city, street, house, room):
        self.city = city
        self.street = street
        self.house = house
        self.room = room

    def __str__(self):
        return self.city + self.street + self.house + self.room

    def get_info(self):
        return self.city, self.street, self.house, self.room
