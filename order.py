from User import *
from Items import *
from datetime import date


class Order:
    __id = 0

    def __init__(self, user: User, item: Item, itemList: ItemList, orderHistory):
        if itemList.is_item_in_shop(item):
            itemList.sell_item(item)
            Order.__id += 1
            self.__id = Order.__id
            self.user = user
            self.item = item
            self.date = date.today()
            orderHistory.add_order(self)
        else:
            print("\nNon-existent product!")


class OrderHistory:

    def __init__(self):
        self.__history = {}
        self.__last_id = 1

    def add_order(self, order: Order):
        id = self.__last_id
        self.__history[id] = order
        self.__last_id += 1

    def print_history(self):
        print('_' * 12)
        print('История заказов:')
        for id, v in self.__history.items():
            print(id, ':', v.user)
            print(v.item)
            print(v.date)
            print()


