from order import *

milk = Item('Milk', 500, 'Fresh')
cheese = Item('Cheese', 1000, 'Germany')
juice = Item('Juice', 100, 'Apple juice')
meet = Item('Meet', 3000, 'Cow"s')
shirt = Item('Shirt', 5000, 'Non-synthetic')
shoes = Item('shoes', 4000, 'No comments')


eatable = Catalog('Eatable')
drink = Catalog('Drink', eatable)
clothes = Catalog('Clothes')

item_list = [milk,
             cheese,
             juice,
             meet]

drink_list = [juice,
              milk]

clothes_list = [shirt,
                shoes]

All_items = ItemList(item_list, eatable)
All_items.add_itemList(drink_list, drink)
All_items.add_itemList(clothes_list, clothes)

address_1 = Address('Moscow', 'Engelsa', '15', '4')
address_2 = Address('Moscow', 'Veri', '1', '14')

user_1 = User('Dima', 'Kashin', address_1, All_items)
user_2 = User('Alex', 'Borgin', address_2, All_items)

history = OrderHistory()

All_items.print_all_list()

o_1 = Order(user_1, shirt, All_items, history)
o_2 = Order(user_2, milk, All_items, history)

history.print_history()
